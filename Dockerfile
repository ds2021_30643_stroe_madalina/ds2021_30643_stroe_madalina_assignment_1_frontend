FROM node:16-alpine AS builder
WORKDIR /app
COPY . .
RUN npm install
ENV NODE_ENV "production"
RUN npm run build

FROM nginx:alpine
ENV PORT 80
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
COPY nginx.conf /etc/nginx/conf.d/default.conf
CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g "daemon off;"