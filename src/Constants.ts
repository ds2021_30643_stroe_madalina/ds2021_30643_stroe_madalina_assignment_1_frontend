const prod = {
    url: {
        API_BASE_URL: 'https://sd-assignment-backend.herokuapp.com',
        RPC_URL: 'https://sd-assignment-rpc-service.herokuapp.com'
    }
};

export const config = prod