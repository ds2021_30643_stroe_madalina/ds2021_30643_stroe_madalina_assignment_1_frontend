export const ApiEndpoints = {
    loginUser: '/api/login/',
    loginAdmin: '/api/login-admin/',
    getUserById: (id: string) => `/api/display-user/${id}`,
    getDeviceById: (id: string) => `/api/display-device/${id}`,
    addUser: '/api/add-user/',
    addDevice: '/api/add-device/',
    addSensor: '/api/add-sensor/',
    addRecord: 'api/add-record/',
    getUsers: '/api/users',
    getDevices: '/api/devices',
    getSensors: '/api/sensors',
    getRecords: '/api/records',
    deleteUser: (id: string) => `/api/delete-user/${id}`,
    deleteDevice: (id: string) => `/api/delete-device/${id}`,
    deleteSensor: (id: string) => `/api/delete-sensor/${id}`,
    deleteRecord: (id: string) => `/api/delete-record/${id}`,
    editUser: (id: string) => `/api/update-user/${id}`,
    editDevice: (id: string) => `/api/update-device/${id}`,
    editSensor: (id: string) => `/api/update-sensor/${id}`,
    editRecord: (id: string) => `/api/update-record/${id}`,
    rpcGetPastComsumption: (deviceId: string, days: string) => '/energy',
    rpcGetBaseline: (clientId: string) => '/energy' 

}