import { BrowserRouter as Router, Redirect, Switch } from 'react-router-dom';
import AddNewDevice from './components/pages/displayItems/displayUsers/AddNewDevice';
import AddNewSensor from './components/pages/displayItems/displayUsers/AddNewSensor';
import AddNewUser from './components/pages/displayItems/displayUsers/AddNewUser';
import UserInfo from './components/pages/displayItems/displayUsers/UserInfo';
import Login from './components/pages/homepage/Login';
import { UserContextProvider } from './components/pages/shared/routes/UserContext';
import Home from './components/pages/selectUser/Home';
import PublicRoute from './components/pages/shared/routes/PublicRoute';
import PrivateRoute from './components/pages/shared/routes/PrivateRoute';
import UserProfile from './components/pages/userAccount/UserProfile';
import UserRoute from './components/pages/shared/routes/UserRoute';
import StackedBar from './components/pages/stackedBar/StackedBar';
import Baseline from './components/pages/stackedBar/Baseline';

const App = () => (
  <UserContextProvider>
    <Router>
      <Switch>
        <PublicRoute path='/home' component={Home} exact />
        <PublicRoute path='/login' component={Login} exact />
        <PrivateRoute path='/user-info' component={UserInfo} exact />
        <PrivateRoute path='/add-new-user' component={AddNewUser} exact />
        <PrivateRoute path='/add-new-device' component={AddNewDevice} exact />
        <PrivateRoute path='/add-new-sensor' component={AddNewSensor} exact />
        <UserRoute path='/user-profile' component={UserProfile} exact />
        <UserRoute path='/bar' component={StackedBar} exact />
        <UserRoute path='/baseline' component={Baseline} exact />

        <Redirect to='/home' />
      </Switch >
    </Router>
  </UserContextProvider>
);
export default App;