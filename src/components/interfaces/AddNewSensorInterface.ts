export interface AddNewSensorInterface {
    description: string;
    maximumValue: number;
    deviceId: number;
};