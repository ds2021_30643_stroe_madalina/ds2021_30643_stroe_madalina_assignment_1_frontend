export interface DeviceInterface {
    id: number;
    description: string;
    maximumEnergyConsumption: number;
    averageEnergyConsumption: number;
    userId: number;
}