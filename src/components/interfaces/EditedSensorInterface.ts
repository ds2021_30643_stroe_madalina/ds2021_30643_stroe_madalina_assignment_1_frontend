export interface EditedSensorInterface {
    id: number;
    description: string;
    maximumValue: number;
    deviceId: number;
}