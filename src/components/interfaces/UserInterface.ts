import { AddressInterface } from "./AddressInterface";

export interface UserInterface {
    id: string;
    name: string;
    username: string;
    address: AddressInterface;
    birthDate: string;
    password: string;
    role: string;
}