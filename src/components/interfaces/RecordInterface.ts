export interface RecordInterface {
    id: number,
    timestamp: string,
    energyConsumption: number,
    sensorId: number
}