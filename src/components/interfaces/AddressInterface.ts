export interface AddressInterface {
    street: string;
    streetNumber: string;
    city: string;
}