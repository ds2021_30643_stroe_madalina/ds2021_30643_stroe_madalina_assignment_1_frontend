export interface EditedUserInterface {
    id: string;
    name: string;
    username: string;
    address: {
        id: string;
        street: string;
        streetNumber: string;
        city: string;
    };
    birthDate: string;
}