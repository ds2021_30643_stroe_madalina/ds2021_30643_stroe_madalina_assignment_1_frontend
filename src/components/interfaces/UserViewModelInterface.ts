import { AddressInterface } from "./AddressInterface";
import { DeviceViewModelInterface } from "./DeviceViewModelInterface";

export interface UserViewModelInterface {
    id: string;
    name: string;
    username: string;
    address: AddressInterface;
    birthDate: string;
    password: string;
    role: string;
    devices: DeviceViewModelInterface[];
}