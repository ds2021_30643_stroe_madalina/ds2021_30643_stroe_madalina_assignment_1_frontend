import { SensorInterface } from "./SensorInterface";

export interface DeviceViewModelInterface{
    id: number;
    description: string;
    maximumEnergyConsumption: number;
    averageEnergyConsumption: number;
    userId: number;
    sensor: SensorInterface;
}