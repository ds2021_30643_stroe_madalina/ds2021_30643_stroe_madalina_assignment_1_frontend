export interface SensorInterface {
    id: number;
    description: string;
    maximumValue: number;
    deviceId: number;
}