export interface AddNewDeviceInterface {
    description: string;
    maximumEnergyConsumption: number;
    averageEnergyConsumption: number;
    userId: number
};