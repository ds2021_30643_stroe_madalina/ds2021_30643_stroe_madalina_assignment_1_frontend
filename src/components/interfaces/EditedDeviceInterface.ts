import { SensorInterface } from "./SensorInterface";

export interface EditedDeviceInterface {
    id: number;
    description: string;
    maximumEnergyConsumption: number;
    averageEnergyConsumption: number;
    userId: number;
    sensor?: SensorInterface;
}