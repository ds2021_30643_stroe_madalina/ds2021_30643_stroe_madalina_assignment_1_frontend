import { Alert, FormControl, InputLabel, MenuItem, OutlinedInput, Select, Snackbar } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { ApiEndpoints } from "../../../../ApiEndpoints";
import { config } from "../../../../Constants";
import { AddNewDeviceInterface } from "../../../interfaces/AddNewDeviceInterface";
import { UserInterface } from "../../../interfaces/UserInterface";
import CustomCard from "../../homepage/layout/CustomCard";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import CustomAddNewContainer from "./layout/CustomAddNewContainer";
import CustomAddNewMainContainer from "./layout/CustomAddNewMainContainer";

const AddNewDevice = () => {
    const { handleSubmit } = useForm();
    const [description, setDescription] = useState('');
    const [maximumEnergyConsumption, setMaximumEnergyConsumption] = useState('');
    const [averageEnergyConsumption, setAverageEnergyConsumption] = useState('');
    const [openError, setOpenError] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [errorMessage, setErrorMessage] = useState('An error has occurred!');
    const [users, setUsers] = useState([]);
    const url = config.url.API_BASE_URL;

    const handleChangeOnDescription = (prop: any) => (event: any) => {
        setDescription(event.target.value);
    };

    const handleChangeOnMaximumEnergyConsumption = (prop: any) => (event: any) => {
        setMaximumEnergyConsumption(event.target.value);
    };

    const handleChangeOnAverageEnergyConsumption = (prop: any) => (event: any) => {
        setAverageEnergyConsumption(event.target.value);
    };

    const onSubmit = () => {
        setDescription('');
        setMaximumEnergyConsumption('');
        setAverageEnergyConsumption('');
        setUserId('');
    };

    const handleClose = (event: any, reason: any) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
        setOpenSuccess(false);
    };

    const addNewDeviceHandler = (values: { description: string, maximumEnergyConsumption: string; averageEnergyConsumption: string, userId: string }) => {
        const newDevice: AddNewDeviceInterface = {
            description: description,
            maximumEnergyConsumption: Number(maximumEnergyConsumption),
            averageEnergyConsumption: Number(averageEnergyConsumption),
            userId: Number(userId)
        }
        if (newDevice.description !== '' && newDevice.maximumEnergyConsumption !== 0 && newDevice.averageEnergyConsumption !== 0) {
            axios.post(`${url}${ApiEndpoints.addDevice}`, newDevice)
                .then(() => {
                    setOpenSuccess(true);
                })
                .catch(() => {
                    setOpenError(true);
                    setErrorMessage('An error has occurred!');
                });
        }
        else {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
        }
    }

    const [userId, setUserId] = useState('');

    const handleChange = (event: any) => {
        setUserId(event.target.value);
    };

    useEffect(() => {
        axios.get(`${url}${ApiEndpoints.getUsers}`)
        .then((response) => {
            const allUsers = JSON.parse(JSON.stringify(response.data));
            const usersList = allUsers.filter((user: any) => user.role === 'user');
            setUsers(usersList.sort((a: any, b: any) => (a.name > b.name) ? 1 : -1));
        })
        .catch(() => {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
        });
    }, [url]);

    return (
        <CustomAddNewMainContainer>
            <h2> Add new device </h2>
            <CustomAddNewContainer>
                <form
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <CustomCard>
                        <div className="card">
                            <div className="row1">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="description"
                                        placeholder="Description"
                                        value={description}
                                        required={true}
                                        onChange={handleChangeOnDescription('description')}
                                    />
                                </CustomLoginFormControl>
                            </div>
                            <div className="row2">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="maximumEnergyConsumption"
                                        placeholder="Maximum Energy Consumption"
                                        value={maximumEnergyConsumption}
                                        required={true}
                                        type='number'
                                        onChange={handleChangeOnMaximumEnergyConsumption('maximumEnergyConsumption')}
                                    />
                                </CustomLoginFormControl>
                            </div>

                            <div className="row3">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="averageEnergyConsumption"
                                        placeholder="Average Energy Consumption"
                                        value={averageEnergyConsumption}
                                        required={true}
                                        type='number'
                                        onChange={handleChangeOnAverageEnergyConsumption('averageEnergyConsumption')}
                                    />
                                </CustomLoginFormControl>
                            </div>

                            <div className="row4">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <FormControl fullWidth>
                                        <InputLabel>User</InputLabel>
                                        <Select
                                            value={userId}
                                            onChange={handleChange}
                                        >
                                            {users.map((user: UserInterface) => <MenuItem value={user.id}>{user.name}</MenuItem>)}
                                        </Select>
                                    </FormControl>
                                </CustomLoginFormControl>
                            </div>

                            <div className="buttonsContainer">
                                <CustomButton type="submit" onClick={() => addNewDeviceHandler({ description, maximumEnergyConsumption, averageEnergyConsumption, userId })} variant="contained">Add</CustomButton>
                                <Link to="user-info"><CustomButton className="goToButton" type="submit">Main page</CustomButton></Link>
                            </div>
                        </div>
                    </CustomCard>
                </form>

                <Snackbar autoHideDuration={2000} onClose={handleClose} open={openError}>
                    <Alert severity="error">{errorMessage}</Alert>
                </Snackbar>

                <Snackbar autoHideDuration={2000} onClose={handleClose} open={openSuccess}>
                    <Alert severity="success">Success!</Alert>
                </Snackbar>
            </CustomAddNewContainer>
        </CustomAddNewMainContainer>
    );
};

export default AddNewDevice;