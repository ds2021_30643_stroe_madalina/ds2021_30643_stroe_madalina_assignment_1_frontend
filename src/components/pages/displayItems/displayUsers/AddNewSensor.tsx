import { Alert, FormControl, InputLabel, MenuItem, OutlinedInput, Select, Snackbar } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { ApiEndpoints } from "../../../../ApiEndpoints";
import { config } from "../../../../Constants";
import { AddNewSensorInterface } from "../../../interfaces/AddNewSensorInterface";
import { DeviceInterface } from "../../../interfaces/DeviceInterface";
import CustomCard from "../../homepage/layout/CustomCard";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import CustomAddNewContainer from "./layout/CustomAddNewContainer";
import CustomAddNewMainContainer from "./layout/CustomAddNewMainContainer";

const AddNewSensor = () => {
    const { handleSubmit } = useForm();
    const [description, setDescription] = useState('');
    const [maximumValue, setMaximumValue] = useState('');
    const [openError, setOpenError] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [errorMessage, setErrorMessage] = useState('An error has occurred!');
    const [devices, setDevices] = useState([]);
    const [deviceId, setDeviceId] = useState('');
    const url = config.url.API_BASE_URL;

    const handleChangeOnDescription = (prop: any) => (event: any) => {
        setDescription(event.target.value);
    };

    const handleChangeOnMaximumValue = (prop: any) => (event: any) => {
        setMaximumValue(event.target.value);
    };

    const onSubmit = () => {
        setDescription('');
        setMaximumValue('');
        setDeviceId('');
    };

    const handleClose = (event: any, reason: any) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
        setOpenSuccess(false);
    };

    const addNewSensorHandler = (values: { description: string, maximumValue: string, deviceId: string }) => {
        const newSensor: AddNewSensorInterface = {
            description: description,
            maximumValue: Number(maximumValue),
            deviceId: Number(deviceId)
        }
        if (newSensor.description !== '' && newSensor.maximumValue !== 0) {
            axios.post(`${url}${ApiEndpoints.addSensor}`, newSensor)
            .then(() => {
                setOpenSuccess(true);
            })
            .catch(() => {
                setOpenError(true);
                setErrorMessage('An error has occurred!');
            });
        }
        else {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
        }
    }

    const handleChange = (event: any) => {
        setDeviceId(event.target.value);
    };

    useEffect(() => {
        axios.get(`${url}${ApiEndpoints.getDevices}`)
        .then((response) => {
            const allDevices = JSON.parse(JSON.stringify(response.data));
            const devicesList = allDevices.filter((device: any) => device.sensor == null);
            setDevices(devicesList.sort((a: any, b: any) => (a.description > b.description) ? 1 : -1));
        })
        .catch(() => {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
        });
    }, [url]);

    return (
        <CustomAddNewMainContainer>
            <h2> Add new sensor </h2>
            <CustomAddNewContainer>
                <form
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <CustomCard>
                        <div className="card">
                            <div className="row1">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="description"
                                        placeholder="Description"
                                        value={description}
                                        required={true}
                                        onChange={handleChangeOnDescription('description')}
                                    />
                                </CustomLoginFormControl>
                            </div>
                            <div className="row2">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="maximumValue"
                                        placeholder="Maximum Value"
                                        value={maximumValue}
                                        type='number'
                                        required={true}
                                        onChange={handleChangeOnMaximumValue('maximumValue')}
                                    />
                                </CustomLoginFormControl>
                            </div>

                            <div className="row3">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <FormControl fullWidth>
                                        <InputLabel>Device</InputLabel>
                                        <Select
                                            value={deviceId}
                                            onChange={handleChange}
                                        >
                                            {devices.map((device: DeviceInterface) => <MenuItem value={device.id}>{device.description}</MenuItem>)}
                                        </Select>
                                    </FormControl>
                                </CustomLoginFormControl>
                            </div>
                            <div className="buttonsContainer">
                                <CustomButton type="submit" onClick={() => addNewSensorHandler({ description, maximumValue, deviceId })} variant="contained">Add</CustomButton>
                                <Link to="user-info"><CustomButton className="goToButton" type="submit">Main page</CustomButton></Link>
                            </div>
                        </div>
                    </CustomCard>
                </form>

                <Snackbar autoHideDuration={2000} onClose={handleClose} open={openError}>
                    <Alert severity="error">{errorMessage}</Alert>
                </Snackbar>

                <Snackbar autoHideDuration={2000} onClose={handleClose} open={openSuccess}>
                    <Alert severity="success">Success!</Alert>
                </Snackbar>
            </CustomAddNewContainer>
        </CustomAddNewMainContainer>
    );
};

export default AddNewSensor;