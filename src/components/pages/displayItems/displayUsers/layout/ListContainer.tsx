import styled from 'styled-components';

const ListContainer = styled.div`
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .MuiList-root {
        display: flex;
        flex-direction: column;
    }
`;

export default ListContainer;