import { Box } from '@mui/material';
import styled from 'styled-components';

const CustomBox = styled(Box)`
    width: 100%;
    .MuiList-root {
        width: 100%;
        display: flex;
        flex-direction: row;
        justify-content: center;
        background: #41939e;
    }

      .MuiListItemIcon-root {
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
        width: 50%;
        margin-right: 15px;
    }

    .MuiListItemText-root {
        color: #fff;
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        width: 50%;
    }

    .MuiListItemButton-root {
        display: flex;
        flex-direction: row;
        justify-content: center
    }

    .MuiListItemButton-root:hover {
        transform: scale(1.2);
        box-shadow: 0 0 5px #fff;
    }

    &.create {
        display: flex;
        justify-content: flex-end;
        align-items: center;

        .createButton {
            margin: 5px;
        }
    }

`;

export default CustomBox;