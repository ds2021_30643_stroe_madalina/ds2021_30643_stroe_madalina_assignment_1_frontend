import { Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';
import DevicesIcon from '@mui/icons-material/Devices';
import SensorsIcon from '@mui/icons-material/Sensors';
import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import UserCard from './UserCard';
import { EditedUserInterface } from '../../../interfaces/EditedUserInterface';
import { DeviceInterface } from '../../../interfaces/DeviceInterface';
import DeviceCard from '../displayDevices/DeviceCard';
import { SensorInterface } from '../../../interfaces/SensorInterface';
import SensorCard from '../displaySensors/SensorCard';
import { EditedDeviceInterface } from '../../../interfaces/EditedDeviceInterface';
import { EditedSensorInterface } from '../../../interfaces/EditedSensorInterface';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { useHistory } from 'react-router-dom';
import CustomButton from '../../shared/CustomButton';
import CustomBox from './layout/CustomBox';
import ListContainer from './layout/ListContainer';
import MainContainer from '../../homepage/layout/MainContainer';
import { UserContext } from '../../shared/routes/UserContext';
import { config } from '../../../../Constants';
import { ApiEndpoints } from '../../../../ApiEndpoints';
import { RecordInterface } from '../../../interfaces/RecordInterface';
import RecordCard from '../displayRecords/RecordCard';

const UserInfo = () => {
    const [users, setUsers] = useState<EditedUserInterface[]>([]);
    const [devices, setDevices] = useState<DeviceInterface[]>([]);
    const [sensors, setSensors] = useState<SensorInterface[]>([]);
    const [records, setRecords] = useState<RecordInterface[]>([]);
    const [onUser, setOnUser] = useState(false);
    const [onDevice, setOnDevice] = useState(false);
    const [onSensor, setOnSensor] = useState(false);
    const [openError, setOpenError] = useState(false);
    const logout = 'LOGOUT';
    const history = useHistory();
    const { setRole } = useContext(UserContext);
    const url = config.url.API_BASE_URL;
    const [disabled, setDisabled] = useState(false);

    const handleCloseSnackbar = (event: any, reason: any) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
    };

    const loadUsers = () => {
        axios.get(`${url}${ApiEndpoints.getUsers}`)
            .then((response) => {
                const allUsers = JSON.parse(JSON.stringify(response.data));
                const usersList = allUsers.filter((user: any) => user.role === 'user');
                setUsers(usersList.sort((a: any, b: any) => (a.name > b.name) ? 1 : -1));
                setDevices([]);
                setSensors([]);
                setRecords([]);
                setOnUser(true);
                setOnDevice(false);
                setOnSensor(false);
                setDisabled(false);
            });
    };

    const loadDevices = () => {
        axios.get(`${url}${ApiEndpoints.getDevices}`)
            .then((response) => {
                const devicesList = JSON.parse(JSON.stringify(response.data));
                setDevices(devicesList.sort((a: any, b: any) => (a.description > b.description) ? 1 : -1));
                setUsers([]);
                setSensors([]);
                setRecords([]);
                setOnUser(false);
                setOnDevice(true);
                setOnSensor(false);
                setDisabled(false);
            });

    }

    const loadSensors = () => {
        axios.get(`${url}${ApiEndpoints.getSensors}`)
            .then((response) => {
                const sensorsList = JSON.parse(JSON.stringify(response.data));
                setSensors(sensorsList.sort((a: any, b: any) => (a.description > b.description) ? 1 : -1));
                setUsers([]);
                setDevices([]);
                setRecords([]);
                setOnUser(false);
                setOnDevice(false);
                setOnSensor(true);
                setDisabled(false);
            });
    }

    const loadRecords = () => {
        axios.get(`${url}${ApiEndpoints.getRecords}`)
            .then((response) => {
                const recordsList = JSON.parse(JSON.stringify(response.data));
                setRecords(recordsList.sort((a: any, b: any) => (a.timestamp > b.timestamp) ? 1 : -1));
                setUsers([]);
                setDevices([]);
                setSensors([]);
                setOnUser(false);
                setOnDevice(false);
                setOnSensor(false);
                setDisabled(true);
            });
    }

    const deleteUserHandler = (item: EditedUserInterface) => {
        axios.delete(`${url}${ApiEndpoints.deleteUser(item.id)}`)
            .then((response) => {
                const result = JSON.parse(JSON.stringify(response.data));
                const usersList = result.filter((user: any) => user.role === 'user');
                const newUsersList = usersList.filter((x: EditedUserInterface) => x.id !== item.id);
                setUsers(usersList.sort((a: any, b: any) => (a.name > b.name) ? 1 : -1));
                setUsers(newUsersList);
            });
    };

    const editUserHandler = (item: EditedUserInterface) => {
        axios.put(`${url}${ApiEndpoints.editUser(item.id)}`, item)
            .then(({ data }) => {
                loadUsers();
                const result = JSON.stringify(data);
                if (result === '0')
                    setOpenError(true);
            });
    };

    const editDeviceHandler = (item: EditedDeviceInterface) => {
        axios.put(`${url}${ApiEndpoints.editDevice(String(item.id))}`, item)
            .then(({ data }) => {
                loadDevices();
                const result = JSON.stringify(data);
                if (result === '0')
                    setOpenError(true);
            });
    };

    const deleteDeviceHandler = (item: EditedDeviceInterface) => {
        axios.delete(`${url}${ApiEndpoints.deleteDevice(String(item.id))}`)
            .then((response) => {
                const devicesList = JSON.parse(JSON.stringify(response.data));
                const newDevicesList = devicesList.filter((x: EditedDeviceInterface) => x.id !== item.id);
                setDevices(newDevicesList);
            });
    };

    const editSensorHandler = (item: EditedSensorInterface) => {
        axios.put(`${url}${ApiEndpoints.editSensor(String(item.id))}`, item)
            .then(({ data }) => {
                loadSensors();
                const result = JSON.stringify(data);
                if (result === '0')
                    setOpenError(true);
            });
    };

    const deleteSensorHandler = (item: EditedSensorInterface) => {
        axios.delete(`${url}${ApiEndpoints.deleteSensor(String(item.id))}`)
            .then((response) => {
                const sensorsList = JSON.parse(JSON.stringify(response.data));
                const newSensorsList = sensorsList.filter((x: EditedSensorInterface) => x.id !== item.id);
                setSensors(newSensorsList);
            });
    };

    const deleteRecordHandler = (item: RecordInterface) => {
        axios.delete(`${url}${ApiEndpoints.deleteRecord(String(item.id))}`)
            .then((response) => {
                const recordsList = JSON.parse(JSON.stringify(response.data));
                const newRecordsList = recordsList.filter((x: RecordInterface) => x.id !== item.id);
                setRecords(newRecordsList);
            });
    };

    const logoutHandler = () => {
        localStorage.clear();
        localStorage.setItem('id', '');
        localStorage.setItem('name', '');
        localStorage.setItem('role', '');
        setRole('');
        history.push('/home');
    };

    const createNewItemHandler = (onUser: boolean, onDevice: boolean, onSensor: boolean) => {
        if (onUser === true && onDevice === false && onSensor === false) {
            history.push('/add-new-user');
        }
        else if (onUser === false && onDevice === true && onSensor === false) {
            history.push('/add-new-device');
        }
        else {
            history.push('/add-new-sensor');
        }
    };

    useEffect(() => {
       loadUsers();
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    return (
        <MainContainer>
            <CustomBox>
                <nav aria-label="main mailbox folders">
                    <List>
                        <ListItem disablePadding>
                            <ListItemButton onClick={loadUsers}>
                                <ListItemIcon>
                                    <PersonIcon />
                                </ListItemIcon>
                                <ListItemText primary="Users" />
                            </ListItemButton>
                        </ListItem>

                        <ListItem disablePadding>
                            <ListItemButton onClick={loadDevices}>
                                <ListItemIcon>
                                    <DevicesIcon />
                                </ListItemIcon>
                                <ListItemText primary="Devices" />
                            </ListItemButton>
                        </ListItem>

                        <ListItem disablePadding>
                            <ListItemButton onClick={loadSensors}>
                                <ListItemIcon>
                                    <SensorsIcon />
                                </ListItemIcon>
                                <ListItemText primary="Sensors" />
                            </ListItemButton>
                        </ListItem>

                        <ListItem disablePadding>
                            <ListItemButton onClick={loadRecords}>
                                <ListItemIcon>
                                    <SensorsIcon />
                                </ListItemIcon>
                                <ListItemText primary="Records" />
                            </ListItemButton>
                        </ListItem>

                        <ListItem disablePadding>
                            <ListItemButton onClick={logoutHandler}>
                                <ListItemIcon>
                                    <AccountCircleIcon />
                                </ListItemIcon>
                                <ListItemText primary={logout} />
                            </ListItemButton>
                        </ListItem>
                    </List>
                </nav>
            </CustomBox>

            <CustomBox className="create">
                <CustomButton disabled={disabled} className="createButton" onClick={() => createNewItemHandler(onUser, onDevice, onSensor)}>CREATE</CustomButton>
            </CustomBox>

            <ListContainer>
                <List>
                    {users.map((user) => <><ListItem key={user.id}>
                        <UserCard item={user} editUserHandler={editUserHandler} deleteUserHandler={() => deleteUserHandler(user)} openError={openError} handleCloseSnackbar={handleCloseSnackbar} />
                    </ListItem><Divider /></>)}

                    {devices.map((device) => <><ListItem key={device.id}>
                        <DeviceCard item={device} editDeviceHandler={editDeviceHandler} deleteDeviceHandler={() => deleteDeviceHandler(device)} />
                    </ListItem><Divider /></>)}

                    {sensors.map((sensor) => <><ListItem key={sensor.id}>
                        <SensorCard item={sensor} editSensorHandler={editSensorHandler} deleteSensorHandler={() => deleteSensorHandler(sensor)} />
                    </ListItem><Divider /></>)}

                    {records.map((record) => <><ListItem key={record.id}>
                        <RecordCard item={record} deleteRecordHandler={() => deleteRecordHandler(record)} />
                    </ListItem><Divider /></>)}
                </List>
            </ListContainer>
        </MainContainer>
    );
};

export default UserInfo;