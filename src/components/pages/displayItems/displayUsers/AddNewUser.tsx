import { Visibility, VisibilityOff } from "@material-ui/icons";
import { Alert, IconButton, InputAdornment, OutlinedInput, Snackbar } from "@mui/material";
import axios from "axios";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { ApiEndpoints } from "../../../../ApiEndpoints";
import { config } from "../../../../Constants";
import { AddressInterface } from "../../../interfaces/AddressInterface";
import CustomCard from "../../homepage/layout/CustomCard";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import CustomAddNewContainer from "./layout/CustomAddNewContainer";
import CustomAddNewMainContainer from "./layout/CustomAddNewMainContainer";

const AddNewUser = () => {
    const { handleSubmit } = useForm();
    const [name, setName] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [street, setStreet] = useState('');
    const [streetNumber, setStreetNumber] = useState('');
    const [city, setCity] = useState('');
    const [usernameRegister, setUsernameRegister] = useState('');
    const [passwordRegister, setPasswordRegister] = useState('');
    const [showPasswordRegister, setShowPasswordRegister] = useState(false);
    const [openError, setOpenError] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [errorMessage, setErrorMessage] = useState('An error has occurred!');
    const url = config.url.API_BASE_URL;

    const handleChangeOnName = (prop: any) => (event: any) => {
        setName(event.target.value);
    };
    const handleChangeOnBirthDate = (prop: any) => (event: any) => {
        setBirthDate(event.target.value);
    };
    const handleChangeOnStreet = (prop: any) => (event: any) => {
        setStreet(event.target.value);
    };
    const handleChangeOnStreetNumber = (prop: any) => (event: any) => {
        setStreetNumber(event.target.value);
    };
    const handleChangeOnCity = (prop: any) => (event: any) => {
        setCity(event.target.value);
    };
    const handleChangeOnUsernameRegister = (prop: any) => (event: any) => {
        setUsernameRegister(event.target.value);
    };

    const handleChangeOnPasswordRegister = (prop: any) => (event: any) => {
        setPasswordRegister(event.target.value);
    };

    const handleClickShowPasswordRegister = () => {
        setShowPasswordRegister(!showPasswordRegister);
    };

    const handleClose = (event: any, reason: any) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
        setOpenSuccess(false);
    };

    const onSubmit = () => {
        setName('');
        setUsernameRegister('');
        setBirthDate('');
        setPasswordRegister('');
        setStreet('');
        setStreetNumber('');
        setCity('');
    };
    const registerButtonHandler = (values: { name: string, username: string; birthDate: string, password: string, address: AddressInterface }) => {
        if (values.name !== '' && values.username !== '' && values.password !== '' && values.birthDate !== '' && values.address.street !== '' && values.address.streetNumber !== '' && values.address.city !== '') {
            axios.post(`${url}${ApiEndpoints.addUser}`, values)
                .then((response) => {
                    if (JSON.parse(JSON.stringify(response.data)) === false) {
                        setOpenError(true);
                        setErrorMessage('An error has occurred!');
                    }
                    else {
                        setOpenSuccess(true);
                    }
                })
                .catch(error => {
                    console.log(error)
                });
        }
        else {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
        }
    }

    return (
        <CustomAddNewMainContainer>
            <h2> Add new user </h2>
            <CustomAddNewContainer>
                <form
                    onSubmit={handleSubmit(onSubmit)}
                >
                    <CustomCard>
                        <div className="card">
                            <div className="row1">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="name"
                                        placeholder="Name"
                                        value={name}
                                        required={true}
                                        onChange={handleChangeOnName('name')}
                                    />
                                </CustomLoginFormControl>

                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="usernameRegister"
                                        placeholder="Username"
                                        value={usernameRegister}
                                        required={true}
                                        onChange={handleChangeOnUsernameRegister('usernameRegister')}
                                    />
                                </CustomLoginFormControl>
                            </div>

                            <div className="row2">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="birthDate"
                                        placeholder="Birth-Date"
                                        value={birthDate}
                                        required={true}
                                        onChange={handleChangeOnBirthDate('birthDate')}
                                    />
                                </CustomLoginFormControl>

                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }} >
                                    <OutlinedInput
                                        id="passwordRegister"
                                        placeholder="Password"
                                        type={showPasswordRegister ? 'text' : 'password'}
                                        value={passwordRegister}
                                        required={true}
                                        onChange={handleChangeOnPasswordRegister('passwordRegister')}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPasswordRegister}
                                                    edge="end"
                                                >
                                                    {showPasswordRegister ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </CustomLoginFormControl>

                            </div>

                            <div className="row3">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="street"
                                        placeholder="Street"
                                        value={street}
                                        required={true}
                                        onChange={handleChangeOnStreet('street')}
                                    />
                                </CustomLoginFormControl>

                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="streetNumber"
                                        placeholder="Street Number"
                                        value={streetNumber}
                                        required={true}
                                        onChange={handleChangeOnStreetNumber('streetNumber')}
                                    />
                                </CustomLoginFormControl>
                            </div>

                            <div className="row4">
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="city"
                                        placeholder="City"
                                        value={city}
                                        required={true}
                                        onChange={handleChangeOnCity('city')}
                                    />
                                </CustomLoginFormControl>
                            </div>
                            <div className="buttonsContainer">
                                <CustomButton type="submit" onClick={() => registerButtonHandler({ name, username: usernameRegister, birthDate, password: passwordRegister, address: { street, streetNumber, city } })} variant="contained">Register</CustomButton>
                                <Link to="user-info"><CustomButton className="goToButton" type="submit">Main page</CustomButton></Link>
                            </div>
                        </div>
                    </CustomCard>
                </form>

                <Snackbar autoHideDuration={2000} onClose={handleClose} open={openError}>
                    <Alert severity="error">{errorMessage}</Alert>
                </Snackbar>

                <Snackbar autoHideDuration={2000} onClose={handleClose} open={openSuccess}>
                    <Alert severity="success">Success!</Alert>
                </Snackbar>
            </CustomAddNewContainer>
        </CustomAddNewMainContainer>
    );
};

export default AddNewUser;