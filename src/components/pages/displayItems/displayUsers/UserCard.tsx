import { Alert, OutlinedInput, Snackbar } from "@mui/material";
import { useState } from "react";
import { EditedUserInterface } from "../../../interfaces/EditedUserInterface";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import ButtonsContainer from "../layout/ButtonsContainer";
import CustomEditCard from "../layout/CustomEditCard";
import CustomListItem from "../layout/CustomListItem";
import CustomModal from "../layout/CustomModel";
import ItemDetailsContainer from "../layout/ItemDetailsContainer";
import MainContainer from "../layout/MainContainer";

export interface UserCardModel {
    item: EditedUserInterface;
    editUserHandler: (value: EditedUserInterface) => void;
    deleteUserHandler: (value: string) => void;
    openError: boolean;
    handleCloseSnackbar: (event: any, reason: any) => void;
}

const UserCard = ({ item, editUserHandler, deleteUserHandler, openError, handleCloseSnackbar }: UserCardModel) => {
    const [open, setOpen] = useState(false);
    const handleOpen = () => {
        setOpen(true);
        setName(item.name);
        setUsername(item.username);
        setBirthDate(item.birthDate);
        setStreet(item.address.street);
        setStreetNumber(item.address.streetNumber);
        setCity(item.address.city);
    }
    const handleClose = () => setOpen(false);
    const [username, setUsername] = useState('');
    const [name, setName] = useState('');
    const [birthDate, setBirthDate] = useState('');
    const [street, setStreet] = useState('');
    const [streetNumber, setStreetNumber] = useState('');
    const [city, setCity] = useState('');

    const handleChangeOnName = (prop: any) => (event: any) => {
        setName(event.target.value);
    };
    const handleChangeOnBirthDate = (prop: any) => (event: any) => {
        setBirthDate(event.target.value);
    };
    const handleChangeOnStreet = (prop: any) => (event: any) => {
        setStreet(event.target.value);
    };
    const handleChangeOnStreetNumber = (prop: any) => (event: any) => {
        setStreetNumber(event.target.value);
    };
    const handleChangeOnCity = (prop: any) => (event: any) => {
        setCity(event.target.value);
    };
    const handleChangeOnUsername = (prop: any) => (event: any) => {
        setUsername(event.target.value);
    };

    const editUser = (values: { id: string, name: string, username: string; birthDate: string, address: { id: string, street: string, city: string, streetNumber: string } }) => {
        if (values.username !== '' && values.name !== '' && values.username !== '' && values.birthDate !== '' && values.address.street !== '' && values.address.streetNumber !== '' && values.address.city !== '') {
            const newUser = { id: item.id, username: username, name: name, address: { id: item.address.id, street: street, streetNumber: streetNumber, city: city }, birthDate: birthDate };
            editUserHandler(newUser);
            setOpen(false);
        }
    }


    return (
        <MainContainer>
            <ItemDetailsContainer>
                <CustomListItem><h2>{item.name}</h2></CustomListItem>
                <CustomListItem>Username:  {item.username}</CustomListItem>
                <CustomListItem>Birth Date: {item.birthDate}</CustomListItem>
            </ItemDetailsContainer>

            <ButtonsContainer>
                <CustomButton onClick={handleOpen}>EDIT</CustomButton>
                <CustomModal
                    open={open}
                    onClose={handleClose}
                >
                    <form
                        className="editForm"
                    >
                        <CustomEditCard>
                            <h2> Edit User </h2>
                            <div className="card">
                                <div className="row1">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="name"
                                            placeholder="Name"
                                            value={name}
                                            label="Name"
                                            required={true}
                                            onChange={handleChangeOnName('name')}
                                        />
                                    </CustomLoginFormControl>

                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="username"
                                            placeholder="Username"
                                            value={username}
                                            label="Username"
                                            required={true}
                                            onChange={handleChangeOnUsername('username')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row2">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="birthDate"
                                            placeholder="Birth-Date"
                                            value={birthDate}
                                            label="Birth Date"
                                            required={true}
                                            onChange={handleChangeOnBirthDate('birthDate')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row3">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="street"
                                            placeholder="Street"
                                            value={street}
                                            label="Street"
                                            required={true}
                                            onChange={handleChangeOnStreet('street')}
                                        />
                                    </CustomLoginFormControl>

                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="streetNumber"
                                            placeholder="Street Number"
                                            value={streetNumber}
                                            label="Street Number"
                                            required={true}
                                            onChange={handleChangeOnStreetNumber('streetNumber')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row4">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="city"
                                            placeholder="City"
                                            value={city}
                                            label="City"
                                            required={true}
                                            onChange={handleChangeOnCity('city')}
                                        />
                                    </CustomLoginFormControl>
                                </div>
                            </div>
                            <CustomButton onClick={() => editUser({ id: item.id, name: name, username: username, birthDate: birthDate, address: { id: item.address.id, street: street, streetNumber: streetNumber, city: city } })} variant="contained">Edit user</CustomButton>
                        </CustomEditCard>
                    </form>
                </CustomModal>
                <CustomButton onClick={() => deleteUserHandler(item.id)}>DELETE</CustomButton>
            </ButtonsContainer>
            <Snackbar autoHideDuration={2000} onClose={handleCloseSnackbar} open={openError}>
                <Alert severity="error">Address already exists</Alert>
            </Snackbar>
        </MainContainer>
    );
};

export default UserCard;