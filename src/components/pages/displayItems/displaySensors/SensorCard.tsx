import { Alert, FormControl, InputLabel, MenuItem, OutlinedInput, Select, Snackbar } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { ApiEndpoints } from "../../../../ApiEndpoints";
import { config } from "../../../../Constants";
import { DeviceInterface } from "../../../interfaces/DeviceInterface";
import { EditedSensorInterface } from "../../../interfaces/EditedSensorInterface";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import ButtonsContainer from "../layout/ButtonsContainer";
import CustomEditCard from "../layout/CustomEditCard";
import CustomListItem from "../layout/CustomListItem";
import CustomModal from "../layout/CustomModel";
import ItemDetailsContainer from "../layout/ItemDetailsContainer";
import MainContainer from "../layout/MainContainer";

interface SensorCardModel {
    item: EditedSensorInterface;
    editSensorHandler: (value: EditedSensorInterface) => void;
    deleteSensorHandler: (value: EditedSensorInterface) => void;
}

const SensorCard = ({ item, editSensorHandler, deleteSensorHandler }: SensorCardModel) => {

    const [description, setDescription] = useState('');
    const [maximumValue, setMaximumValue] = useState(0);
    const [devices, setDevices] = useState([]);
    const [deviceId, setDeviceId] = useState('');
    const [openError, setOpenError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('An error has occurred!');
    const [open, setOpen] = useState(false);
    const url = config.url.API_BASE_URL;

    const handleOpen = () => {
        setOpen(true);
        setDescription(item.description);
        setMaximumValue(item.maximumValue);
        setDeviceId(String(item.deviceId));
    }
    const handleClose = () => setOpen(false);

    const handleChangeOnDescription = (prop: any) => (event: any) => {
        setDescription(event.target.value);
    };
    const handleChangeOnMaximumValue = (prop: any) => (event: any) => {
        setMaximumValue(event.target.value);
    };

    const editSensor = (values: { id: number, description: string, maximumValue: number, deviceId: number }) => {
        if (values.id !== 0 && values.description !== '' && values.maximumValue !== 0) {
            const newSensor = { id: Number(item.id), description: description, maximumValue: Number(maximumValue), deviceId: Number(values.deviceId) };
            editSensorHandler(newSensor);
            setOpen(false);
        }
    }

    const handleChange = (event: any) => {
        setDeviceId(event.target.value);
    };

    const getDevices = () => {
        axios.get(`${url}${ApiEndpoints.getDevices}`)
        .then((response) => {
            const allDevices = JSON.parse(JSON.stringify(response.data));
            const devicesList = allDevices.filter((device: any) => device.sensor === null || device.sensor.id === item.id);
            setDevices(devicesList.sort((a: any, b: any) => (a.description > b.description) ? 1 : -1));
        })
        .catch(() => {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
        });
    }

    useEffect(() => {
        getDevices();
    });

    return (
        <MainContainer>
            <ItemDetailsContainer>
                <CustomListItem><h2>{item.description}</h2></CustomListItem>
                <CustomListItem>Maximum value:  {item.maximumValue}</CustomListItem>
            </ItemDetailsContainer>

            <ButtonsContainer>
                <CustomButton onClick={handleOpen}>EDIT</CustomButton>
                <CustomModal
                    open={open}
                    onClose={handleClose}
                >
                    <form
                        className="editForm"
                    >
                        <CustomEditCard>
                            <h2> Edit Device </h2>
                            <div className="card">
                                <div className="row1">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="description"
                                            value={description}
                                            label="Description"
                                            required={true}
                                            onChange={handleChangeOnDescription('description')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row2">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="maximumValue"
                                            value={maximumValue}
                                            label="Maximum Value"
                                            type='number'
                                            required={true}
                                            onChange={handleChangeOnMaximumValue('maximumValue')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row3">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <FormControl fullWidth>
                                            <InputLabel>Device</InputLabel>
                                            <Select
                                                value={deviceId}
                                                onChange={handleChange}
                                            >
                                                {devices.map((device: DeviceInterface) => <MenuItem value={device.id}>{device.description}</MenuItem>)}
                                            </Select>
                                        </FormControl>
                                    </CustomLoginFormControl>
                                </div>
                            </div>
                            <CustomButton onClick={() => editSensor({ id: item.id, description: description, maximumValue: maximumValue, deviceId: Number(deviceId) })} variant="contained">Edit sensor</CustomButton>
                        </CustomEditCard>
                    </form>
                </CustomModal>
                <CustomButton onClick={() => deleteSensorHandler(item)}>DELETE</CustomButton>

            </ButtonsContainer>

            <Snackbar autoHideDuration={2000} onClose={handleClose} open={openError}>
                <Alert severity="error">{errorMessage}</Alert>
            </Snackbar>
        </MainContainer>
    );
};

export default SensorCard;