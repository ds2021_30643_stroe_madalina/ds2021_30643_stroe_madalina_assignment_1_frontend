import { RecordInterface } from "../../../interfaces/RecordInterface";
import CustomButton from "../../shared/CustomButton";
import ButtonsContainer from "../layout/ButtonsContainer";
import CustomListItem from "../layout/CustomListItem";
import ItemDetailsContainer from "../layout/ItemDetailsContainer";
import MainContainer from "../layout/MainContainer";

interface RecordCardModel {
    item: RecordInterface;
    deleteRecordHandler: (value: RecordInterface) => void;
}

const RecordCard = ({ item, deleteRecordHandler }: RecordCardModel) => {
    return (
        <MainContainer>
            <ItemDetailsContainer>
                <CustomListItem><h2>{item.timestamp}</h2></CustomListItem>
                <CustomListItem>Energy Consumption:  {item.energyConsumption}</CustomListItem>
            </ItemDetailsContainer>

            <ButtonsContainer>
                <CustomButton onClick={() => deleteRecordHandler(item)}>DELETE</CustomButton>

            </ButtonsContainer>
        </MainContainer>
    );
};

export default RecordCard;