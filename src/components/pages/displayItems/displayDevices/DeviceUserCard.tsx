import { OutlinedInput } from "@mui/material";
import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { ApiEndpoints } from "../../../../ApiEndpoints";
import { config } from "../../../../Constants";
import { EditedDeviceInterface } from "../../../interfaces/EditedDeviceInterface";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import ButtonsContainer from "../layout/ButtonsContainer";
import CustomEditCard from "../layout/CustomEditCard";
import CustomListItem from "../layout/CustomListItem";
import CustomModal from "../layout/CustomModel";
import ItemDetailsContainer from "../layout/ItemDetailsContainer";
import MainContainer from "../layout/MainContainer";

interface DeviceCardModel {
    item: EditedDeviceInterface;
}

const DeviceUserCard = ({ item }: DeviceCardModel) => {
    const [openConsumption, setOpenConsumption] = useState(false);
    const [totalConsumption, setTotalConsumption] = useState(0);
    const [averageConsumption, setAverageEnergyConsumption] = useState(0);
    const url = config.url.API_BASE_URL;
    const history = useHistory();

    const handleCloseConsumption = () => setOpenConsumption(false);

    const storeDeviceId = (deviceId: number) => {
        localStorage.setItem("deviceId", String(deviceId));
        history.push('/bar');
    }

    const energyConsumption = (deviceId: number) => {
        axios.post(`${url}${ApiEndpoints.getDeviceById(String(deviceId))}`, deviceId)
        .then(({ data }) => {
            const result = JSON.parse(JSON.stringify(data));
            const records = result.sensor.records;
            let sum = 0;
            let numberOfElements = records.length;
            records.forEach((record: { energyConsumption: number }) => {
                sum += Number(record.energyConsumption);

            });
            setTotalConsumption(sum);
            if(numberOfElements>0)
                setAverageEnergyConsumption(sum/numberOfElements);
            setOpenConsumption(true);
        })
        .catch((error) => {
            console.log(error);
        });

    }

    return (
        <MainContainer>
            <ItemDetailsContainer>
                <CustomListItem><h2>{item.description}</h2></CustomListItem>
                <CustomListItem>Maximum energy consumption:  {item.maximumEnergyConsumption}</CustomListItem>
                <CustomListItem>Average energy consumption {item.averageEnergyConsumption}</CustomListItem>
            </ItemDetailsContainer>

            <ButtonsContainer>
                <CustomButton onClick={() => energyConsumption(item.id)}>Consumption</CustomButton>
                <CustomModal
                    open={openConsumption}
                    onClose={handleCloseConsumption}
                >
                    <form
                        className="editForm"
                    >
                        <CustomEditCard>
                            <h2> Energy Consumption </h2>
                            <div className="card">
                                <div className="row1">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="averageConsumption"
                                            label="Average Consumption"
                                            disabled={true}
                                            value={averageConsumption}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row2">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="totalConsumption"
                                            label="Total Consumption"
                                            disabled={true}
                                            value={totalConsumption}
                                        />
                                    </CustomLoginFormControl>
                                </div>
                            </div>
                        </CustomEditCard>
                    </form>
                </CustomModal>

                <CustomButton onClick={() => storeDeviceId(item.id)}>MORE</CustomButton>

            </ButtonsContainer>
        </MainContainer>
    );
};

export default DeviceUserCard;