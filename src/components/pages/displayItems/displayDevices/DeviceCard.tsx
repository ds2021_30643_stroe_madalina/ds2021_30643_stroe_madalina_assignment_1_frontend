import { Alert, FormControl, InputLabel, MenuItem, OutlinedInput, Select, Snackbar } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { ApiEndpoints } from "../../../../ApiEndpoints";
import { config } from "../../../../Constants";
import { EditedDeviceInterface } from "../../../interfaces/EditedDeviceInterface";
import { UserInterface } from "../../../interfaces/UserInterface";
import CustomButton from "../../shared/CustomButton";
import CustomLoginFormControl from "../../shared/CustomLoginFormControl";
import ButtonsContainer from "../layout/ButtonsContainer";
import CustomEditCard from "../layout/CustomEditCard";
import CustomListItem from "../layout/CustomListItem";
import CustomModal from "../layout/CustomModel";
import ItemDetailsContainer from "../layout/ItemDetailsContainer";
import MainContainer from "../layout/MainContainer";

interface DeviceCardModel {
    item: EditedDeviceInterface;
    editDeviceHandler: (value: EditedDeviceInterface) => void;
    deleteDeviceHandler: (value: EditedDeviceInterface) => void;
}

const DeviceCard = ({ item, editDeviceHandler, deleteDeviceHandler }: DeviceCardModel) => {
    const [description, setDescription] = useState('');
    const [maximumEnergyConsumption, setMaximumEnergyConsumption] = useState(0);
    const [averageEnergyConsumption, setAverageEnergyConsumption] = useState(0);
    const [open, setOpen] = useState(false);
    const [openError, setOpenError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('An error has occurred!');
    const url = config.url.API_BASE_URL;

    const handleOpen = () => {
        setOpen(true);
        setDescription(item.description);
        setMaximumEnergyConsumption(item.maximumEnergyConsumption);
        setAverageEnergyConsumption(item.averageEnergyConsumption);
        setUserId(String(item.userId));

    }
    const handleClose = () => setOpen(false);

    const handleChangeOnDescription = (prop: any) => (event: any) => {
        setDescription(event.target.value);
    };
    const handleChangeOnMaximumEnergyConsumption = (prop: any) => (event: any) => {
        setMaximumEnergyConsumption(event.target.value);
    };

    const handleChangeOnAverageEnergyConsumption = (prop: any) => (event: any) => {
        setAverageEnergyConsumption(event.target.value);
    };

    const editDevice = (values: { id: number, description: string, maximumEnergyConsumption: number, averageEnergyConsumption: number, userId: number }) => {
        if (values.id !== 0 && values.description !== '' && values.maximumEnergyConsumption !== 0 && values.averageEnergyConsumption !== 0) {
            const newDevice = { id: Number(item.id), description: description, maximumEnergyConsumption: Number(maximumEnergyConsumption), averageEnergyConsumption: Number(averageEnergyConsumption), userId: Number(userId) };
            if (newDevice) {
                editDeviceHandler(newDevice);
                setOpen(false);
            }
        }
    }
    const getUsers = () => {
        axios.get(`${url}${ApiEndpoints.getUsers}`)
            .then((response) => {
                const allUsers = JSON.parse(JSON.stringify(response.data));
                const usersList = allUsers.filter((user: any) => user.role === 'user');
                setUsers(usersList.sort((a: any, b: any) => (a.name > b.name) ? 1 : -1));
            })
            .catch(() => {
                setOpenError(true);
                setErrorMessage('An error has occurred!');
            });
    };

    const [userId, setUserId] = useState('');
    const [users, setUsers] = useState([]);

    const handleChange = (event: any) => {
        setUserId(event.target.value);
    };

    useEffect(() => {
        getUsers();
    });


    return (
        <MainContainer>
            <ItemDetailsContainer>
                <CustomListItem><h2>{item.description}</h2></CustomListItem>
                <CustomListItem>Maximum energy consumption:  {item.maximumEnergyConsumption}</CustomListItem>
                <CustomListItem>Average energy consumption {item.averageEnergyConsumption}</CustomListItem>
            </ItemDetailsContainer>

            <ButtonsContainer>
                <CustomButton onClick={handleOpen}>EDIT</CustomButton>
                <CustomModal
                    open={open}
                    onClose={handleClose}
                >
                    <form
                        className="editForm"
                    >
                        <CustomEditCard>
                            <h2> Edit Device </h2>
                            <div className="card">
                                <div className="row1">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="description"
                                            value={description}
                                            label="Description"
                                            required={true}
                                            onChange={handleChangeOnDescription('description')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row2">

                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="maximumEnergyConsumption"
                                            value={maximumEnergyConsumption}
                                            label="Maximum Energy"
                                            required={true}
                                            type='number'
                                            onChange={handleChangeOnMaximumEnergyConsumption('maximumEnergyConsumption')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row3">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <OutlinedInput
                                            id="averageEnergyConsumption"
                                            value={averageEnergyConsumption}
                                            label="Average Energy"
                                            type='number'
                                            required={true}
                                            onChange={handleChangeOnAverageEnergyConsumption('averageEnergyConsumption')}
                                        />
                                    </CustomLoginFormControl>
                                </div>

                                <div className="row4">
                                    <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                    >
                                        <FormControl fullWidth>
                                            <InputLabel>User</InputLabel>
                                            <Select
                                                value={userId}
                                                onChange={handleChange}
                                            >
                                                {users.map((user: UserInterface) => <MenuItem value={user.id}>{user.name}</MenuItem>)}
                                            </Select>
                                        </FormControl>
                                    </CustomLoginFormControl>
                                </div>

                            </div>
                            <CustomButton onClick={() => editDevice({ id: item.id, description: description, maximumEnergyConsumption: maximumEnergyConsumption, averageEnergyConsumption: averageEnergyConsumption, userId: Number(userId) })} variant="contained">Edit device</CustomButton>
                        </CustomEditCard>
                    </form>
                </CustomModal>

                <CustomButton onClick={() => deleteDeviceHandler(item)}>DELETE</CustomButton>
            </ButtonsContainer>

            <Snackbar autoHideDuration={2000} onClose={handleClose} open={openError}>
                <Alert severity="error">{errorMessage}</Alert>
            </Snackbar>

        </MainContainer>
    );
};

export default DeviceCard;