import styled from "styled-components";

const ButtonsContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;

    .MuiButtonBase-root {
        height: 20%;
        margin: 10px;
    }
`;
export default ButtonsContainer;