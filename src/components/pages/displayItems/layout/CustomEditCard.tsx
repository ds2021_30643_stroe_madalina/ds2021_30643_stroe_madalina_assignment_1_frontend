import { Card } from "@mui/material";
import styled from "styled-components";

const CustomEditUserCard = styled(Card)`
  &.MuiPaper-root {
    box-shadow: #ADD8E6 0px 10px 15px -3px, #ADD8E6 0px 4px 6px -2px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 100%;
    width: 100%;

    .MuiButton-contained {
        height: 5vh;
        margin-top: 20px;
    }

    &.statisticsCard {
      padding-bottom: 50px;
      padding-top: 20px;

    }
  } 
`;
export default CustomEditUserCard;