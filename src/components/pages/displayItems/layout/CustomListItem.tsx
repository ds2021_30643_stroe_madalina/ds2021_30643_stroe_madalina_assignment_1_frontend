import styled from "styled-components";

const CustomListItem = styled.div`
    height: 100%;
    padding: 10px;
`;
export default CustomListItem;