import { Modal } from "@mui/material";
import styled from "styled-components";

const CustomModal = styled(Modal)`
  span {
      visibility: visible;
  }
    .row1, .row2, .row3, .row4{
        display: flex;
        flex-direction: row;
        justify-content: center;
        padding: 10px;
    }
    .editForm {
        height: 70%;
        width: 70%;
    }
    overflow: auto;
`;
export default CustomModal;