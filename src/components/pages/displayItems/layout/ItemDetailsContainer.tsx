import styled from "styled-components";

const ItemDetailsContainer = styled.div`
    width: 100%;
    padding: 10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
`;
export default ItemDetailsContainer;