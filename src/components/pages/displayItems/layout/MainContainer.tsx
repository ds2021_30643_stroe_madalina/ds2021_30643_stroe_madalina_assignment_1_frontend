import styled from "styled-components";

const MainContainer = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    height: 100%;
    justify-content: center;
`;
export default MainContainer;