import { Card, CardActions, CardContent, Paper, Typography } from "@material-ui/core";
import { Avatar } from "@mui/material";
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import CustomButton from "../shared/CustomButton";
import { Divider, List, ListItem, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import CustomBox from "../displayItems/displayUsers/layout/CustomBox";
import { useHistory } from "react-router";
import { UserContext } from "../shared/routes/UserContext";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ListContainer from "../displayItems/displayUsers/layout/ListContainer";
import { DeviceViewModelInterface } from "../../interfaces/DeviceViewModelInterface";
import { UserViewModelInterface } from "../../interfaces/UserViewModelInterface";
import DeviceUserCard from "../displayItems/displayDevices/DeviceUserCard";
import CustomModal from "../displayItems/layout/CustomModel";
import CustomEditCard from "../displayItems/layout/CustomEditCard";

import {
    ArgumentAxis,
    ValueAxis,
    Chart,
    BarSeries,
  } from '@devexpress/dx-react-chart-material-ui';
import { config } from "../../../Constants";
import { ApiEndpoints } from "../../../ApiEndpoints";  
  
const CustomLeftDiv = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

const CustomRightDiv = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    overflox: scroll;
`;

const MainContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

const CustomUserDetailsCard = styled(Card)`
    width: 100%;
    height: 60%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;

    &.userDetails { 
        box-shadow: none;
    }
`;

const CustomCardContent = styled(CardContent)`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
    width: 100%;
    height: 100%;
    border: none;
`;

const UserProfile = () => {

    const userId = localStorage.getItem('id');
    const name = localStorage.getItem('name');
    const [user, setUser] = useState<UserViewModelInterface>({ id: '', name: '', username: '', birthDate: '', password: '', address: { street: '', streetNumber: '', city: '' }, role: '', devices: [] });
    const [devices, setDevices] = useState([]);
    const logout = 'LOGOUT';
    const history = useHistory();
    const { setRole } = useContext(UserContext);
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);
    const url = config.url.API_BASE_URL;
    
    const stringAvatar = (name: string) => {
        return {
            children: `${name.split(' ')[0][0]}`,
        };
    };

    const logoutHandler = () => {
        localStorage.clear();
        localStorage.setItem('id', '');
        localStorage.setItem('name', '');
        localStorage.setItem('role', '');
        setRole('');
        history.push('/home');
    };

    const openStatisticsModal = () => {
        setOpen(true);
    };

    useEffect(() => {
        axios.post(`${url}${ApiEndpoints.getUserById(String(userId))}`, userId)
        .then(({ data }) => {
            const result: UserViewModelInterface = JSON.parse(JSON.stringify(data));
            const devices1: DeviceViewModelInterface[] = result.devices;
            setUser(result);
            setDevices(JSON.parse(JSON.stringify(devices1)));
        })
        .catch(error => {
            console.log(error)
        });
    }, [userId, url]);


    return (
        <MainContainer>
            <CustomLeftDiv>
                <CustomUserDetailsCard className="userDetails">
                    <CustomCardContent>
                        <Avatar sx={{ bgcolor: '#00838f' }} {...stringAvatar(`${name}`)} />
                        <Typography variant="h6" component="div">
                            Username: {user.username}
                        </Typography>
                        <Typography >
                            Name: {user.name}
                        </Typography>

                        <Typography >
                            Birth Date: {user.birthDate}
                        </Typography>

                        <Typography >
                            Password: {user.password}
                        </Typography>

                        <Typography >
                            Street: {user.address.street}
                        </Typography>

                        <Typography >
                            Street Number: {user.address.streetNumber}
                        </Typography>

                        <Typography >
                            City: {user.address.city}
                        </Typography>
                    </CustomCardContent>

                    <CardActions>
                        <CustomButton size="medium" color="primary" onClick={() => openStatisticsModal()}>View statistics</CustomButton>
                        <CustomModal
                            open={open}
                            onClose={handleClose}
                        >
                            <form
                                className="editForm"
                            >
                                <CustomEditCard className="statisticsCard">
                                    <h2> Energy consumption </h2>
                                    <Paper>
                                        <Chart
                                            data={devices}
                                        >
                                            <ArgumentAxis />
                                            <ValueAxis />

                                            <BarSeries valueField="maximumEnergyConsumption" argumentField="description" />
                                        </Chart>
                                    </Paper>

                                </CustomEditCard>
                            </form>
                        </CustomModal>
                    </CardActions>
                </CustomUserDetailsCard>
            </CustomLeftDiv>

            <CustomRightDiv>
                <CustomBox>
                    <nav aria-label="main mailbox folders">
                        <List >
                            <ListItem disablePadding>
                                <ListItemButton onClick={logoutHandler}>
                                    <ListItemIcon>
                                        <AccountCircleIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={logout} />
                                </ListItemButton>
                            </ListItem>
                        </List>
                    </nav>
                </CustomBox>

                <ListContainer>
                    <List>

                        {devices.map((device) => <><ListItem key={device}>
                            <DeviceUserCard item={device} />
                        </ListItem><Divider /></>)}
                    </List>
                </ListContainer>
            </CustomRightDiv>
        </MainContainer>
    );
};

export default UserProfile;