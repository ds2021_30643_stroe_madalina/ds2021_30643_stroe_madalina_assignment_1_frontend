import { Card } from '@mui/material';
import styled from 'styled-components';

const CustomLoginAdminCard = styled(Card)`
  &.MuiPaper-root {
    box-shadow: #ADD8E6 0px 10px 15px -3px, #ADD8E6 0px 4px 6px -2px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 50vh;
    width: 30vw;
    padding: 10px;
    overflow: auto;

    .MuiButton-contained {
        margin-top: 30px;
        height: 10%;
    }
  } 
`;
export default CustomLoginAdminCard;