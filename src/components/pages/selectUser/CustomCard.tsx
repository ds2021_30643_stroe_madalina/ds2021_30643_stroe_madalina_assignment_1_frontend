import { Card } from '@mui/material';
import styled from 'styled-components';

const CustomCard = styled(Card)`
  &.MuiPaper-root {
    box-shadow: #ADD8E6 0px 1px 0px, #ADD8E6 0px 0px 8px;    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    height: 40%;
    width: 50%;

    .adminButton {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    .userButton {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;

        .MuiButtonBase-root {
            height: 100%;
        }
    }

  } 
`;
export default CustomCard;