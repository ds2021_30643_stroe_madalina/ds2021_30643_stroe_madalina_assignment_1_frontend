import { Avatar } from '@mui/material';
import styled from 'styled-components';

const CustomAvatar = styled(Avatar)`
    margin: 20px;
`;
export default CustomAvatar;