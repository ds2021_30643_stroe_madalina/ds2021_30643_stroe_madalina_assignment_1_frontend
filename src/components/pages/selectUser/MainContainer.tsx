import styled from 'styled-components';

const MainContainer = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;

    .MuiSnackbar-root {
        width: 100%;
        display: flex;
        flex-direction: column;
    }
`;
export default MainContainer;