import { Alert, IconButton, InputAdornment, Modal, OutlinedInput, Snackbar } from '@mui/material';
import CustomButton from '../shared/CustomButton';
import UserAvatar from '../../../assets/images/user.png';
import AdminAvatar from '../../../assets/images/admin.png';
import { Link, useHistory } from "react-router-dom";
import { useContext, useState } from 'react';
import '../../../index.css';
import CustomLoginFormControl from '../shared/CustomLoginFormControl';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import axios from 'axios';
import MainContainer from './MainContainer';
import CustomCard from './CustomCard';
import CustomAvatar from './CustomAvatar';
import CustomLoginAdminCard from './CustomLoginAdminCard';
import { UserContext } from '../shared/routes/UserContext';
import { config } from '../../../Constants';
import { ApiEndpoints } from '../../../ApiEndpoints';

const Home = () => {
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const history = useHistory();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    const [openError, setOpenError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('An error has occurred!');
    const { setRole } = useContext(UserContext);
    const url = config.url.API_BASE_URL;

    const handleCloseSnackbar = (event: any, reason: any) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenError(false);
    };

    const handleChangeOnUsername = (prop: any) => (event: any) => {
        setUsername(event.target.value);
    };

    const handleChangeOnPassword = (prop: any) => (event: any) => {
        setPassword(event.target.value);
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const loginAdminHandler = (values: { username: string; password: string }) => {
        if (values.username !== '' && values.password !=='') {
            axios.post(`${url}${ApiEndpoints.loginAdmin}`, values)
                .then(({ data }) => {
                    const result = JSON.parse(JSON.stringify(data));

                    if (result.username == null) {
                        setOpenError(true);
                        setErrorMessage('Wrong credentials');
                    }
                    else {
                        const user1 = {username: result.username, role: result.role };
                        localStorage.setItem('username', user1.username);
                        localStorage.setItem('role', user1.role);
                        setRole(user1.role);
                        history.push("/user-info");
                    }
                })
                .catch(error => {
                    console.log(error)
                });
        }
        else {
            setOpenError(true);
            setErrorMessage('Please fill the fields in order to login');
        }
    }

    return (
        <div>
            <MainContainer>
                <CustomCard>
                    <div className="adminButton">
                        <CustomAvatar
                            src={AdminAvatar}
                            sx={{ width: 70, height: 70 }}
                            sizes="xs"
                        />
                        <CustomButton onClick={handleOpen}>Admin</CustomButton>
                        <Modal
                            open={open}
                            onClose={handleClose}
                        >
                            <CustomLoginAdminCard>

                                <h2> Login Admin </h2>
                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                                >
                                    <OutlinedInput
                                        id="username"
                                        placeholder="Username"
                                        value={username}
                                        required={true}
                                        onChange={handleChangeOnUsername('username')}
                                    />
                                </CustomLoginFormControl>

                                <CustomLoginFormControl sx={{ m: 1, width: '25ch' }} >
                                    <OutlinedInput
                                        id="password"
                                        placeholder="Password"
                                        type={showPassword ? 'text' : 'password'}
                                        value={password}
                                        required={true}
                                        onChange={handleChangeOnPassword('password')}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </CustomLoginFormControl>

                                <CustomButton onClick={() => loginAdminHandler({ username: username, password: password })} variant="contained">Login</CustomButton>
                            </CustomLoginAdminCard>
                        </Modal>

                    </div>

                    <div className="userButton">
                        <CustomAvatar
                            variant='circular'
                            src={UserAvatar}
                            sx={{ width: 70, height: 70 }}
                        />
                        <Link to='login'><CustomButton>User</CustomButton></Link>
                    </div>
                </CustomCard>

                <Snackbar autoHideDuration={2000} onClose={handleCloseSnackbar} open={openError}>
                    <Alert severity="error">{errorMessage}</Alert>
                </Snackbar>
            </MainContainer>
        </div>
    );
};
export default Home;