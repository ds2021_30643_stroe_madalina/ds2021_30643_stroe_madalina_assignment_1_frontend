import styled from 'styled-components';
import RegisterLogo from '../../../../assets/images/registerLogo.jpg';

const RightContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  background-position: center;
  background-size: 90% auto;
  background-image: url(${RegisterLogo});
  margin-top: 100px;
`;
export default RightContainer;