import { Card } from '@mui/material';
import styled from 'styled-components';

const CustomCard = styled(Card)`
  &.MuiPaper-root {
    box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
  } 
  
  .card {
    margin: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .buttonsContainer {
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
  }
`;
export default CustomCard;