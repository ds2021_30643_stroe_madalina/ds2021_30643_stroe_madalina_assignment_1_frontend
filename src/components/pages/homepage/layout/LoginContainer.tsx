import styled from 'styled-components';

const LoginContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;

  .leftDiv {
    width: 50%;
  }

  .rightDiv {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 50%;
  }
`;
export default LoginContainer;