import styled from 'styled-components';

const LeftContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 100px;

  .row1 {
    display: flex;
    flex-direction: row;
  }

  .row2 {
    display: flex;
    flex-direction: row;
  }

  .row3 {
    display: flex;
    flex-direction: row;
  }

  .row4 {
    display: flex;
    flex-direction: column;

    .MuiButtonBase-root {
      width: 80%;
      margin: 20px auto;
    }
  }
`;

export default LeftContainer;