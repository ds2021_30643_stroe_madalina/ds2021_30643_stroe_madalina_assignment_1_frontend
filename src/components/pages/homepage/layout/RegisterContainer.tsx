import styled from 'styled-components';

const RegisterContainer = styled.div`
  height: 650px;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;
export default RegisterContainer;