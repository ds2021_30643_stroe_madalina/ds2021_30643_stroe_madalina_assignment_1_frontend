import { Snackbar } from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { Alert, IconButton, InputAdornment, OutlinedInput } from '@mui/material';
import axios from 'axios';
import { useContext, useState } from 'react';
import { useHistory } from "react-router-dom";
import { UserInterface } from '../../interfaces/UserInterface';
import { AddressInterface } from '../../interfaces/AddressInterface';
import { useForm } from "react-hook-form";
import CustomLoginFormControl from '../shared/CustomLoginFormControl';
import CustomButton from '../shared/CustomButton';
import MainContainer from './layout/MainContainer';
import LoginContainer from './layout/LoginContainer';
import RegisterContainer from './layout/RegisterContainer';
import LeftContainer from './layout/LeftContainer';
import RightContainer from './layout/RightContainer';
import CustomCard from './layout/CustomCard';
import { UserContext } from '../shared/routes/UserContext';
import { config } from '../../../Constants';
import { ApiEndpoints } from '../../../ApiEndpoints';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const history = useHistory();
  const { handleSubmit } = useForm();
  const [name, setName] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [street, setStreet] = useState('');
  const [streetNumber, setStreetNumber] = useState('');
  const [city, setCity] = useState('');
  const [usernameRegister, setUsernameRegister] = useState('');
  const [passwordRegister, setPasswordRegister] = useState('');
  const [showPasswordRegister, setShowPasswordRegister] = useState(false);
  const [openError, setOpenError] = useState(false);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState('An error has occurred!');
  const { setRole } = useContext(UserContext);
  const url = config.url.API_BASE_URL;

  const handleChangeOnName = (prop: any) => (event: any) => {
    setName(event.target.value);
  };
  const handleChangeOnBirthDate = (prop: any) => (event: any) => {
    setBirthDate(event.target.value);
  };
  const handleChangeOnStreet = (prop: any) => (event: any) => {
    setStreet(event.target.value);
  };
  const handleChangeOnStreetNumber = (prop: any) => (event: any) => {
    setStreetNumber(event.target.value);
  };
  const handleChangeOnCity = (prop: any) => (event: any) => {
    setCity(event.target.value);
  };
  const handleChangeOnUsernameRegister = (prop: any) => (event: any) => {
    setUsernameRegister(event.target.value);
  };

  const handleChangeOnPasswordRegister = (prop: any) => (event: any) => {
    setPasswordRegister(event.target.value);
  };

  const handleChangeOnUsername = (prop: any) => (event: any) => {
    setUsername(event.target.value);
  };

  const handleChangeOnPassword = (prop: any) => (event: any) => {
    setPassword(event.target.value);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleClickShowPasswordRegister = () => {
    setShowPasswordRegister(!showPasswordRegister);
  };

  const handleClose = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenError(false);
    setOpenSuccess(false);
  };

  const onSubmit = () => {
    setName('');
    setUsernameRegister('');
    setBirthDate('');
    setPasswordRegister('');
    setStreet('');
    setStreetNumber('');
    setCity('');
  };

  const loginButtonHandler = (values: { username: string; password: string }) => {
    if(values.username !== '' && values.password !== ''){
      axios.post(`${url}${ApiEndpoints.loginUser}`, values)
      .then(({ data }) => {
        const result = JSON.parse(JSON.stringify(data));

        if (result.name === null || result.role ==="admin") {
          setOpenError(true);
          setErrorMessage('Wrong credentials');
        }
        else {
          const address: AddressInterface = {
            street: result.address.street,
            streetNumber: result.address.streetNumber,
            city: result.address.city
          };

          const user1: UserInterface = { id: result.id, name: result.name, username: result.username, password: result.password, address: address, role: result.role, birthDate: result.birthDate };
          localStorage.setItem('id', user1.id);
          localStorage.setItem('name', user1.name);
          localStorage.setItem('role', user1.role);
          setRole(user1.role);
          history.push("/user-profile");
        }
      })
      .catch(error => {
        console.log(error)
      });
    }
    else{
      setOpenError(true);
      setErrorMessage('Please fill the fields in order to login');
    }
  }

  const registerButtonHandler = (values: { name: string, username: string; birthDate: string, password: string, address: AddressInterface }) => {
    if (values.name !== '' && values.username !== '' && values.password !== '' && values.birthDate !== '' && values.address.street !== '' && values.address.streetNumber !== '' && values.address.city !== '') {
      axios.post(`${url}${ApiEndpoints.addUser}`, values)
        .then((response) => {
          if (JSON.parse(JSON.stringify(response.data)) === false) {
            setOpenError(true);
            setErrorMessage('An error has occurred!');
          }
          else {
            setOpenSuccess(true);
          }
        })
        .catch(error => {
          console.log(error)
        });
    }
    else {
      setOpenError(true);
      setErrorMessage('An error has occurred!');
    }
  }

  return (
    <MainContainer >
      <LoginContainer>
        <div className="leftDiv"></div>
        <div className="rightDiv">
          <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
          >
            <OutlinedInput
              id="username"
              placeholder="Username"
              value={username}
              required={true}
              onChange={handleChangeOnUsername('username')}
            />
          </CustomLoginFormControl>

          <CustomLoginFormControl sx={{ m: 1, width: '25ch' }} >
            <OutlinedInput
              id="password"
              placeholder="Password"
              type={showPassword ? 'text' : 'password'}
              value={password}
              required={true}
              onChange={handleChangeOnPassword('password')}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </CustomLoginFormControl>
          <CustomButton onClick={() => loginButtonHandler({ username: username, password: password })} variant="contained">Login</CustomButton>
        </div>
      </LoginContainer>

      <form
        onSubmit={handleSubmit(onSubmit)}
      >
        <RegisterContainer>
          <LeftContainer>
            <CustomCard>
              <div className="card">
                <div className="row1">
                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                  >
                    <OutlinedInput
                      id="name"
                      placeholder="Name"
                      value={name}
                      required={true}
                      onChange={handleChangeOnName('name')}
                    />
                  </CustomLoginFormControl>

                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                  >
                    <OutlinedInput
                      id="usernameRegister"
                      placeholder="Username"
                      value={usernameRegister}
                      required={true}
                      onChange={handleChangeOnUsernameRegister('usernameRegister')}
                    />
                  </CustomLoginFormControl>
                </div>

                <div className="row2">
                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                  >
                    <OutlinedInput
                      id="birthDate"
                      placeholder="Birth-Date"
                      value={birthDate}
                      required={true}
                      onChange={handleChangeOnBirthDate('birthDate')}
                    />
                  </CustomLoginFormControl>

                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }} >
                    <OutlinedInput
                      id="passwordRegister"
                      placeholder="Password"
                      type={showPasswordRegister ? 'text' : 'password'}
                      value={passwordRegister}
                      required={true}
                      onChange={handleChangeOnPasswordRegister('passwordRegister')}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPasswordRegister}
                            edge="end"
                          >
                            {showPasswordRegister ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </CustomLoginFormControl>

                </div>

                <div className="row3">
                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                  >
                    <OutlinedInput
                      id="street"
                      placeholder="Street"
                      value={street}
                      required={true}
                      onChange={handleChangeOnStreet('street')}
                    />
                  </CustomLoginFormControl>

                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                  >
                    <OutlinedInput
                      id="streetNumber"
                      placeholder="Street Number"
                      value={streetNumber}
                      required={true}
                      onChange={handleChangeOnStreetNumber('streetNumber')}
                    />
                  </CustomLoginFormControl>
                </div>

                <div className="row4">
                  <CustomLoginFormControl sx={{ m: 1, width: '25ch' }}
                  >
                    <OutlinedInput
                      id="city"
                      placeholder="City"
                      value={city}
                      required={true}
                      onChange={handleChangeOnCity('city')}
                    />
                  </CustomLoginFormControl>
                  <CustomButton type="submit" onClick={() => registerButtonHandler({ name, username: usernameRegister, birthDate, password: passwordRegister, address: { street, streetNumber, city } })} variant="contained">Register</CustomButton>
                </div>
              </div>
            </CustomCard>
          </LeftContainer>
          <RightContainer />
        </RegisterContainer>
      </form>

      <Snackbar autoHideDuration={2000} onClose={handleClose} open={openError}>
        <Alert severity="error">{errorMessage}</Alert>
      </Snackbar>

      <Snackbar autoHideDuration={2000} onClose={handleClose} open={openSuccess}>
        <Alert severity="success">Successful registration!</Alert>
      </Snackbar>
    </MainContainer>
  );
};
export default Login;

