import { Button } from '@material-ui/core';
import styled from 'styled-components';

const CustomButton = styled(Button)`
  &.MuiButtonBase-root {
    height: 80%;
    background: #41939e;
    color: #fff;
  }

  &.goToButton {
    height: 100%;
  }
`;
export default CustomButton;