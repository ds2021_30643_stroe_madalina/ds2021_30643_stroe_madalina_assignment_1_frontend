import { FormControl } from "@mui/material";
import styled from 'styled-components';

const CustomLoginFormControl = styled(FormControl)`
  &.MuiFormControl-root {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
  }
`;
export default CustomLoginFormControl;