import React, { useContext } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { UserContext } from './UserContext';

interface PrivateRouteProps extends Omit<RouteProps, "component"> {
    component: React.ElementType;
    // any additional vars
}

const UserRoute: React.FC<PrivateRouteProps> = ({ component: Component, ...rest }) => {
    const { role } = useContext(UserContext);
    return (
        <Route
            {...rest}
            render={(routeProps) => {
                if (role==='admin' || !role) return <Redirect to='/home' />;

                return <Component {...routeProps} />;
            }}
        />
    );
};

export default UserRoute;
