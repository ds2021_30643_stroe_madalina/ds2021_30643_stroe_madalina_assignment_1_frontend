import React, { createContext, useState } from 'react';

interface UserContextModel {
  role: any;
  setRole: (value: string) => void;
}

export const UserContext = createContext<UserContextModel>({ role: 'initial', setRole: () => {}});

export const UserContextProvider = ({ children }: {children: any}) => {
  const [role, setRole] = useState(localStorage.getItem('role'));
  
  return <UserContext.Provider value={{role, setRole}}>
    {children}
  </UserContext.Provider>;
};