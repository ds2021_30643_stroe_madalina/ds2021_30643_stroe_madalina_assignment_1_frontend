import React, { useContext } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { UserContext } from './UserContext';

interface PrivateRouteProps extends Omit<RouteProps, "component"> {
    component: React.ElementType;
    // any additional vars
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({ component: Component, ...rest }) => {
    const { role } = useContext(UserContext);
    return (
        <Route
            {...rest}
            render={(routeProps) => {
                if (!role) return <Redirect to='/home' />;
                if (role==='user') return <Redirect to='/user-profile' />;

                return <Component {...routeProps} />;
            }}
        />
    );
};

export default PrivateRoute;
