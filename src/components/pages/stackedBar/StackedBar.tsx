import { useEffect, useState } from 'react';
import {
    BarChart, Bar, XAxis, YAxis,
    CartesianGrid
} from 'recharts';
import styled from "styled-components";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import axios from 'axios';
import { config } from '../../../Constants';
import { ApiEndpoints } from '../../../ApiEndpoints';
import CustomButton from '../shared/CustomButton';
import { Link } from 'react-router-dom';

const MainBarContainer = styled.div`
    height: 100vh;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

const CustomBox = styled(Box)`
    margin: 20px;
`;

interface HourlyInterface { 
    hour: number;
    values: number[];
};

const StackedBar = () => {
    const [day, setDay] = useState(1);
    const [dailyConsumption, setDailyConsumption] = useState([false, true, true, true, true, true, true]);
    const url = config.url.RPC_URL;
    const [consumption, setConsumption] = useState<HourlyInterface[]>([]);
    const clientId = localStorage.getItem('deviceId');

    const handleChange = (event: any) => {
        getPastConsumption(Number(clientId), event.target.value);

        setDay(event.target.value);
        let visibleDays = dailyConsumption.fill(false, 0, event.target.value);
        visibleDays = dailyConsumption.fill(true, event.target.value, dailyConsumption.length);
        setDailyConsumption(visibleDays);
    }

    const getPastConsumption = (deviceId: number, days: number) => {
        const parameters: number[] = [deviceId, days];

        axios.post(`${url}${ApiEndpoints.rpcGetPastComsumption(String(deviceId), String(days))}`,  {"jsonrpc": "2.0", "method": "GetPastConsumption", "params": parameters, "id": 1})
        .then(({ data }) => {
            const result = JSON.parse(JSON.stringify(data)).result;
            const myObject = Object.values(result);
            const hourlyArray: HourlyInterface[] = [];

            for(let i=0; i< myObject.length; i++) {
                const aux: any = myObject[i];
                
                const hourlyConsumption: HourlyInterface = { 
                    hour: i,
                    values: Object.values(aux)
                };
                
                hourlyArray.push(hourlyConsumption);
            }
            setConsumption(hourlyArray);

        })
        .catch((error) => {
            console.log(error);
        });
    };

    useEffect(() => {
        getPastConsumption(Number(clientId), day);
       });

    return (
        <MainBarContainer>
            <h2>Stacked bar chart for consumption</h2>
            <Link to="baseline"><CustomButton>Baseline consumption</CustomButton></Link>
            <CustomBox sx={{ minWidth: 120 }}>
                <FormControl fullWidth>
                    <InputLabel>Day</InputLabel>
                    <Select
                        value={day}
                        label="Day"
                        onChange={handleChange}
                    >
                        <MenuItem value={1}>Day 1</MenuItem>
                        <MenuItem value={2}>Day 2</MenuItem>
                        <MenuItem value={3}>Day 3</MenuItem>
                        <MenuItem value={4}>Day 4</MenuItem>
                        <MenuItem value={5}>Day 5</MenuItem>
                        <MenuItem value={6}>Day 6</MenuItem>
                        <MenuItem value={7}>Day 7</MenuItem>

                    </Select>
                </FormControl>
            </CustomBox>

            <BarChart width={1000} height={600} data={consumption} >
                <CartesianGrid />
                <XAxis dataKey="hour" />
                <YAxis />
                
                <Bar dataKey="values[0]" hide={dailyConsumption[0]} stackId="a" fill="#b5a28d" />
                <Bar dataKey="values[1]" hide={dailyConsumption[1]} stackId="a" fill="#c7c75f" />
                <Bar dataKey="values[2]" hide={dailyConsumption[2]} stackId="a" fill="#3f7824" />
                <Bar dataKey="values[3]" hide={dailyConsumption[3]} stackId="a" fill="#427a6c" />
                <Bar dataKey="values[4]" hide={dailyConsumption[4]} stackId="a" fill="#5683a6" />
                <Bar dataKey="values[5]" hide={dailyConsumption[5]} stackId="a" fill="#6553b5" />
                <Bar dataKey="values[6]" hide={dailyConsumption[6]} stackId="a" fill="#780e31" />
            </BarChart>
        </MainBarContainer>
    );
};

export default StackedBar;