import { useEffect, useState } from 'react';
import {
    BarChart, Bar, XAxis, YAxis,
    CartesianGrid
} from 'recharts';
import styled from "styled-components";
import axios from 'axios';
import { config } from '../../../Constants';
import { ApiEndpoints } from '../../../ApiEndpoints';
import CustomButton from '../shared/CustomButton';
import { Link } from 'react-router-dom';

const MainBarContainer = styled.div`
    height: 100vh;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

interface BaselineConsumption {
    hour: number;
    baselineValue: number;
}

const Baseline = () => {
    const url = config.url.RPC_URL;
    const energyArray: BaselineConsumption[] = [];
    const [energy, setEnergy] = useState<BaselineConsumption[]>([]);
    const clientId = localStorage.getItem('deviceId');

    const getBaseline = (clientId: number) => {
        axios.post(`${url}${ApiEndpoints.rpcGetBaseline(String(clientId))}`, { "jsonrpc": "2.0", "method": "GetBaseline", "params": [`${clientId}`], "id": 1 })
            .then(({ data }) => {
                const result = JSON.parse(JSON.stringify(data)).result;
                const myObject = Object.values(result);

                for (let i = 0; i < myObject.length; i++) {
                    const aux: BaselineConsumption = {
                        hour: i,
                        baselineValue: Number(myObject[i])
                    }
                    energyArray.push(aux);
                }

                setEnergy(energyArray);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    useEffect(() => {
        getBaseline(Number(clientId));
    });

    return (
        <MainBarContainer>
            <h2>Baseline consumption</h2>
            <Link to="bar"><CustomButton>Energy Consumption</CustomButton></Link>
            <BarChart width={1000} height={600} data={energy} >
                <CartesianGrid />
                <XAxis dataKey="hour" />
                <YAxis />
                <Bar dataKey="baselineValue" stackId="a" fill="#5683a6" />
            </BarChart>
        </MainBarContainer>
    );
};

export default Baseline;